function back_propagate(layer,fi)

global b beta delta V w

dg_db = beta * (1 - tanh(beta * b{layer}) .^ 2);

if fi
    delta{layer} = dg_db * (fi - V{layer+1});
else
    delta{layer} = dg_db .* (w{layer+1}' * delta{layer+1});
end

if layer > 1
    back_propagate(layer-1,0)
end