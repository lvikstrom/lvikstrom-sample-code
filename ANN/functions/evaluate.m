function energy = evaluate(data)

global V

nSets = [size(data{1},1); size(data{2},1)];

H = @(o,idx) ((o - data{idx}(:,end))' * (o - data{idx}(:,end))) / 2;
C = @(o,idx) sum(abs(data{idx}(:,end) - sign(o))) / nSets(idx);

energy = zeros(2,2);
for set = 1:2
    fi = zeros(nSets(set),1);
    for eval = 1 : nSets(set)
        V{1} = data{set}(eval,1:end-1)';
        propagate(1)
        fi(eval) = V{end};
    end
    energy(set,:) = [H(fi,set) C(fi,set)];
end