function [w,theta] = initialize_weights_theta(nNeurons)

% rows of w means weights leading to 1 neuron 
%    e.g. number of cols = number of units in prev layer
% columns is from each input unit (variable)

nShapes = length(nNeurons);
w = cell(nShapes-1,1);
theta = cell(nShapes-1,1);
for i = 1:nShapes-1
    w{i} = -0.2 + 2*0.2*rand(nNeurons(i+1),nNeurons(i));
    theta{i} = -1 + 2*rand(nNeurons(i+1),1);
end

