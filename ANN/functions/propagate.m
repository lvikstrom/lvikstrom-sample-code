function propagate(layer)

global b w V beta theta nLayers

b{layer} = (w{layer} * V{layer}) - theta{layer};
V{layer+1} = tanh(beta * b{layer});

if layer < nLayers
    propagate(layer+1)
end

end
