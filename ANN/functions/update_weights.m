function update_weights(layer,eta)

global delta theta V w

d = eta * delta{layer};
dw = d * V{layer}';
w{layer} = w{layer} + dw;
theta{layer} = theta{layer} - d;

if layer > 1, update_weights(layer-1,eta), end