%% import traning and validation data from 'new' data set
clear, clc
addpath('data','functions')
data = {load('train_data_new.txt') load('valid_data_new.txt')};
global b beta delta nLayers theta V w

% ------------ Normalize ------------
means = mean(data{1}(:,1:end-1));
sigmas = std(data{1}(:,1:end-1));

% ------------ Model size -----------
indata_variables = size(data{1},2) - 1;
nNeurons = [3 2 1];
nLayers = length(nNeurons);

% ---------- Model params -----------
b = cell(nLayers,1); beta = 0.5; delta = cell(nLayers,1); 
eta = 0.01; V = cell(nLayers,1);
% ---------- Iteration spec ---------
nIterations = 1e5; % 1e5 seems to be a lower limit for this data set
nTrainingRuns = 1;
nTrainingSets = size(data{1},1);

% -------- Energy and classification plot specifications -------
sample_freq = nIterations/1000;
sample_rate = 200;
plot_x_vec = 1:sample_rate:nIterations;
performance_evolution = zeros(2,nIterations/sample_rate,2);
energy_evolution_store = {};

% --------- Normalize data -----------
norm_data = cellfun(@(x) [(x(:,1:end-1) - repmat(means,size(x,1),1)) ./ ...
    repmat(sigmas,size(x,1),1) x(:,end)],data,'UniformOutput',0);
training_data = norm_data{1};
validation_data = norm_data{2};
% ---------- DO WORK -----------
tic
for trainIdx = 1:nTrainingRuns

    [w, theta] = initialize_weights_theta([indata_variables nNeurons]);
    
    % we train with 1 random sample at the time to get stochastisity
    % and avoid local minimas.
    choice = 1 + fix(nTrainingSets*rand(nIterations,1));

    for i = 1:nIterations
        
        V{1} = training_data(choice(i),1:end-1)';
        fi = training_data(choice(i),end);
        propagate(1)
        back_propagate(nLayers,fi)
        update_weights(nLayers,eta)
        
        if mod(i,sample_rate) == 1
            idx = fix(i/sample_rate)+1;
            last_evaluation = evaluate(norm_data);
            performance_evolution(:,idx,:) = last_evaluation;
        end
        
    end

    energy_evolution_store{trainIdx} = performance_evolution(:,1:idx,:);

end
fprintf('Finished training network over %d runs in %.2f s\n', ...
    nIterations,toc)

% -------------- Plot Contour Energy And Error -----------------

h3=figure(1); clf, axis square,hold on
titles = {'Energy','Classification Error'};
for i = 1:2 % energy, classification error
        subplot(2,3,3*i), hold all, axis square
        h1 = cellfun(@(x,clr,idx) plot(1:sample_rate:size(x,2)*sample_rate, x(1,:,i),'b'), ...
            energy_evolution_store,'UniformOutput',0); % train 
        h2 = cellfun(@(x) plot(1:sample_rate:size(x,2)*sample_rate, x(2,:,i),'g'), ...
            energy_evolution_store,'UniformOutput',0); % validate, 
        title(titles{i})
        xlabel('Iter')
        legend([h1{1} h2{1}],'training','validation','Location','NE')
end

% -------- Plot data set and network classifier line -------
n = 80;
x = linspace(-2,2,n);
Z = [];
for i = 1:n
    for j = 1:n
        V{1} = [x(i); x(j)];
        propagate(1)
        Z(j,i) = V{end}; % Z(y,x) due to matrix number repr
    end
end 

subplot(2,3,[1 2 4 5]), hold on
gscatter(training_data(:,1), training_data(:,2), training_data(:,end))
contour(x,x,Z,[0 0]);
gscatter(validation_data(:,1), validation_data(:,2), validation_data(:,end))
contour(x,x,Z,[0 0]);
legend('-1','1','Location','NE')
