import numpy as np
from numpy.random import rand
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

keys = ['infected', 'susceptible', 'recovered']

resident_map = {
            'susceptible': 'new_susceptible',
            'infected': 'new_infected',
            'recovered': 'new_recovered',
        }
        
color_map = {'susceptible': 'g',
            'infected': 'k',
            'recovered': 'r'
            }

class Location():

    def __init__(self, pos, gamma=0.005, beta=0.2, diffusion_rate=0.3, 
        grid_size=100, initial_infection_rate=0.1, initial_state=None):
        self.pos = pos
        self.gamma = gamma
        self.beta = beta
        self.diffusion_rate = diffusion_rate
        self.grid_size = grid_size
        self.residents = {
            'susceptible': 0,
            'infected': 0,
            'recovered': 0,
        }        
        self.new_residents = {
            'new_susceptible': 0,
            'new_infected': 0,
            'new_recovered': 0,
        }
        if not initial_state:
            self.initial_state = 'susceptible'
            if rand() < initial_infection_rate:
                self.initial_state = 'infected'
        else:
            self.initial_state = initial_state
        
        self.residents[self.initial_state] +=1

    @property
    def neighbours(self):
        n = self.grid_size
        return self.pos + np.round(np.array([
            -n + (self.pos < n) * n**2, # north
            n - (self.pos > (n**2 - n)) * n**2, # south
            1 - (np.mod(self.pos + 1, n) == 0) * n, # east
            -1 + (np.mod(self.pos, n) == 0) * n # west
            ]))
    def new_resident(self,attr):
        self.new_residents[resident_map[attr]] += 1

    @property
    def get_number_of_residents(self,):
        return sum(self.residents.values())

    def move_residents(self):
        new_locations = {}
        for k in keys:
            n_movers = sum(rand(self.residents[k]) > self.diffusion_rate)
            if n_movers > 0:
                destinations = [self.neighbours[idx]
                    for idx in np.random.choice(4, n_movers)]
                new_locations[k] = destinations
                self.residents[k] -= len(destinations)

        return new_locations
    
    def flush_new_residents(self):
        for k in self.new_residents.keys():
            self.new_residents[k] = 0
        
    def update_resident_count(self):
        for k,v in resident_map.items():
            self.residents[k] += self.new_residents[v]

    def live_life(self,):
        self.update_resident_count()
        self.flush_new_residents()
        new_infected = 0
        
        if any(rand(self.residents['infected']) < self.beta):
            new_infected = self.residents['susceptible']
            self.residents['susceptible'] = 0

        # recover (the newly infected can't recover)
        recovering = sum(rand(self.residents['infected']) < self.gamma)
        self.residents['recovered'] += recovering
        self.residents['infected'] += new_infected - recovering

    @property
    def get_residents(self):
        return [self.residents[k] for k in keys]


class Society():

    def __init__(self, grid_size=100, agent_probability=0.1, 
        n_agents=None, initial_infection_rate=0.1, **kvargs):
        
        self.grid_size = grid_size
        self.places = {}
        self.position_dict = {}
        self.time_data = np.empty([0,3])
        self.location_kvargs = kvargs
        
        start_state = lambda x: 'infected' if x else 'susceptible'

        if not n_agents:
            n_agents = grid_size**2 * agent_probability

        positions = np.random.choice(grid_size**2, n_agents, replace=False)

        if initial_infection_rate:
            n_initial_infected = grid_size**2 * agent_probability \
            * initial_infection_rate
            infecteds = np.random.choice(positions,
                int(n_initial_infected), replace=False)

        for position in positions:
            infected = any(position == infecteds) # this is unnecessary slow
            new_location = Location(
                position,
                grid_size=self.grid_size,
                initial_state=start_state(infected),
                **kvargs)    

            self.add_place(position, new_location.initial_state)
            self.places[position] = new_location

    def add_place(self, position, state):
        self.position_dict.setdefault(state,set()).add(position)

    def live_life(self):
        for pos,location in self.places.items():
            location.live_life()
            for k in keys:
                if location.residents[k] == 0:
                    self.position_dict.get(k,set()).discard(location.pos)
                else:
                    self.position_dict.get(k,set()).add(location.pos)

            if location.get_number_of_residents == 0:
                # remove from lists here
                del self.places[pos]

    def move_around(self,):
        for pos,location in self.places.items():
            new_locations = location.move_residents()

            for state,destinations in new_locations.items():
                for destination in destinations:
                    if destination in self.places:
                        self.places[destination].new_resident(state)
                    else:
                        self.places[destination] = Location(destination,
                            initial_state=state, grid_size=self.grid_size,
                            **self.location_kvargs)
                    self.add_place(destination,state)
    @property
    def get_habitant_count(self,):
        values = np.zeros(3)
        for pos,location in self.places.items():
            values += location.get_residents
        return values

    def store_statistics(self):
        self.time_data = np.vstack((self.time_data,
            self.get_habitant_count))

    def get_scatter_data(self,state):
        data = self.position_dict.get(state,())
        x = []
        y = []
        for pos in data:
            x.append(np.floor_divide(pos,self.grid_size))
            y.append(np.remainder(pos-1,self.grid_size))
        return (x,y)

    @property
    def diffusion_rate(self,):
        return self.places.values()[0].diffusion_rate

    @property
    def beta(self,):
        return self.places.values()[0].beta

    @property
    def gamma(self,):
        return self.places.values()[0].gamma


class Plotter():

    def __init__(self, agent_probability=0.1, tMax=100, grid_size=100,
        betas=None, R0=None, society=None, **kvargs):
        
        self.fig, ax = plt.subplots(1,len(kvargs))
        self.agent_probability = agent_probability
        self.grid_size = grid_size
        self.society = society
        self.handles = {}

        if len(kvargs) == 1:
            self.ax = [ax]
        else:
            self.ax = ax

        for idx,plot_type in enumerate(kvargs):
            if plot_type =='scatter':
                self.handles['scatter'] = {}
                self.ax[idx].set_xlim([0, grid_size])
                self.ax[idx].set_ylim([0, grid_size])
                
                for k in keys:
                    self.handles['scatter'][k], = self.ax[idx].plot(
                        [], [], 'o', label=k, color = color_map[k])
                self.ax[idx].legend()
            
            elif plot_type == 'time':
                self.handles['time'] = {}
                self.ax[idx].set_xlim([0, tMax])
                self.ax[idx].set_ylim([0, 1])
                self.ax[idx].set_xlabel('Time')
                self.ax[idx].set_ylabel('Population Density')
                self.ax[idx].set_title('$d = %s$ $beta=%s$ $gamma=%s$'
                    % (society.diffusion_rate, society.beta, society.gamma))
                for k in keys:
                    self.handles['time'][k], = self.ax[idx].plot(
                        [], [], '-', label = k, color = color_map[k])
                self.ax[idx].legend()
            elif plot_type == 'phase':
                self.handles['phase'] = {}
                self.betas = betas
                self.R0 = R0
                self.ax[idx].set_xlim([0, max(R0)])
                self.ax[idx].set_ylim([0, 1])
                self.ax[idx].set_xlabel('$R_0$')
                self.ax[idx].set_ylabel('$R_{\inf}$')
                for b in betas:
                    self.handles['phase'][b], = self.ax[idx].plot(
                        [], [], '-o', label = '$beta=%s$' % b)
                self.ax[idx].legend()
            elif plot_type == 'phase3D':
                self.ax = self.fig.gca(projection='3d')
                self.handles['phase'] = {}
                self.R0, self.betas = np.meshgrid(R0, betas)
                self.ax.set_xlim(0, max(R0))
                self.ax.set_ylim(0, max(betas))
                self.ax.set_zlim(0,1)
                self.ax.set_xlabel('$R_0$')
                self.ax.set_ylabel('$beta$')
                self.ax.set_zlabel('$R_{\inf}$')
                self.handles['phase3D'] = self.ax.plot_surface(
                    self.R0,self.betas, np.zeros([len(R0),len(betas)]), 
                    rstride=1, cstride=1, linewidth=0, antialiased=False)

    def update_scatter(self,society, store_statistics=False):
        for k in keys:
            h = self.handles['scatter'][k]
            if not store_statistics:
                data = society.get_scatter_data(k)
            else:
                old_data = h.get_data()
                new_data = society.get_scatter_data(k)
                data = (np.append(old_data[0],new_data[0]),
                    np.append(old_data[1],new_data[1]))
            h.set_data(data)
        plt.pause(0.01)

    def update_time(self,society, update_freq=50):

        time_data = np.divide(society.time_data,
            self.agent_probability * self.grid_size**2)
        t = np.arange(0,len(time_data)*update_freq,update_freq)
        for idx,k in enumerate(keys):
            self.handles['time'][k].set_data(t,time_data[:,idx].T) 
        plt.pause(0.01)

    def update_phase(self,data):
        for idx,b in enumerate(self.betas):
            self.handles['phase'][b].set_data(self.R0,data[idx,:])
        plt.pause(0.01)

    def update_phase3D(self,data):
        self.handles['phase3D'].remove()
        self.handles['phase3D'] = self.ax.plot_surface(self.R0, 
            self.betas, data, rstride=1, cstride=1, linewidth=0,
            antialiased=False)
        plt.pause(0.01)



