from agent_model import (
    Location,
    Society,
    Plotter
)    
import time
import numpy as np


def assignment_11():
    # random walk
    tMax = 50
    n = 20
    society = Society(grid_size=n, n_agents=1,agent_probability=0.01)
    plotter = Plotter(grid_size=n,scatter=True)

    for t in xrange(tMax):
        society.move_around()
        society.live_life()
        plotter.update_scatter(society,store_statistics=True)
        time.sleep(0.1)
    plotter.ax[0].set_title('Random walk for $t=%s$ steps' % tMax)
    plotter.fig.savefig('assignment_11.pdf',format='pdf',dpi = 1000)

def assignment_12(n=30,name='assignment_12.pdf',beta=0.2,gamma=0.005,
        tMax = 5000, t_plot = 300, update_freq = 10):
    
    society = Society(grid_size=n, agent_probability=0.1, beta=beta,
        gamma=gamma)
    plotter = Plotter(grid_size=n, scatter=True, time=True, tMax=tMax, 
        society=society)
    
    for t in xrange(tMax):
        society.move_around()
        society.live_life()
        if np.mod(t,update_freq) == 0:
            print 'Finished evaluating iteration: %s  / %s' % (t,tMax)
            plotter.update_time(society,update_freq=update_freq)
            society.store_statistics()
            if society.get_habitant_count[0] == 0:
                if t < t_plot:
                    plotter.ax[0].set_title(
                        'Disease spreading at $t=%s$' % t_plot)
                    plotter.update_scatter(society)
                break

        if t == t_plot:
            plotter.ax[0].set_title('Disease spreading at $t=%s$' % t_plot)
            plotter.update_scatter(society)


    plotter.fig.savefig(name,format='pdf',dpi = 1000)

def assignment_13():
    assignment_12(n=100,name='assignment_13.pdf')

def assignment_21():
    assignment_12(n=100, gamma=0.1, name='assignment_21.pdf',
        tMax=400,update_freq=1,t_plot=1)

def assignment_22():    
    assignment_12(n=100, beta=0.6, name='assignment_22.pdf',
        tMax=2000,update_freq=10,t_plot=500)


def iterate(R0=None, betas=None, reps=10, plotter_update=None):
    if not any(R0):
        R0 = np.linspace(0.001,20,10)
    if not any(betas):
        betas = np.linspace(0.05,0.55,10)
    
    tMax = 5000
    update_freq = 1
    recovered = np.zeros([len(betas),len(R0)])
    for i,b in enumerate(betas):
        for j,gamma in enumerate(b/R0):
            for r in xrange(reps):
                society = Society(grid_size=20, agent_probability=0.5, beta=b,
                    gamma=gamma, initial_infection_rate=0.01)
                for t in xrange(tMax):
                    society.move_around()
                    society.live_life()
                    if np.mod(t,update_freq) == 0:
                        habitants = society.get_habitant_count
                        if habitants[0] == 0: # no infected
                            break
                print 'Evaluated beta = %s (%s / %s), gamma = %s (%s / %s) ' \
                    '@ iter %s / %s in %s steps' % (b,i,len(betas),gamma,j,
                        len(R0),r,reps,t)
                recovered[i,j] += habitants[2] / sum(habitants)
            recovered[i,j] *= 1.0/reps
            if plotter_update:
                plotter_update(recovered)
    return recovered


def assignment_31(name=None, beta = [0.2, 0.5], reps = 10):
    plotter = Plotter(R0=R0,betas=beta, phase=phase, phase3D=phase3D)
    data = iterate(betas=beta, plotter_update=plotter.update_phase)
    plotter.fig.savefig('assignment_31.pdf',format='pdf',dpi = 1000)

def assignment_41():
    betas=np.linspace(0.05,0.55,10)
    R0 = np.linspace(0.001,20,10)
    plotter = Plotter(R0=R0, betas=betas, phase3D=True)
    data = iterate(R0=R0, betas=betas, reps=15)
    plotter.update_phase3D(data)
    #time.sleep(10)
    
    plotter.fig.savefig('assignment_41.pdf',format='pdf',dpi = 1000)


if __name__ == '__main__':
    #assignment_11()
    #assignment_12()
    #assignment_13()
    assignment_21()
    assignment_22()
    #assignment_31()
    #assignment_41()