function [ quiscent_csc_SF, quiscent_cc_SF , csc_SF, cc_SF ] = SF( D )
%Surviving fraction of radiation dose D Gy (Gray)
global  xi lambda a b

LQ_term = ( a*D+b*D^2 );

quiscent_csc_SF= exp(- xi * lambda * LQ_term );
quiscent_cc_SF = exp(- xi * 1 * LQ_term );

csc_SF         = exp(- 1 * lambda * LQ_term );
cc_SF          = exp(- 1 * 1 * LQ_term );
end
