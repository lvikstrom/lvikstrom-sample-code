function state = cell_action(position, action, varargin)

global lattice stem_value next new_locations neighbourvec next_position
state = 0;
switch action
    case 'remove'
        %n_active_locations = n_active_locations - 1;
        lattice(position) = 0;
    case 'get_neighbour'
        %state = ~ all(lattice(get_neighbours()));
        state = get_neighbour_location();
    case 'move'
        move_cell()
    case 'add'
        lattice(position) = varargin{1};
        %n_active_locations = n_active_locations + 1;
        keep()
    case 'keep'
        keep()
    case 'stem'
        state = lattice(position) >= stem_value;
    case 'split'
        split_cell(0)
    case 'differentiate'
        split_cell(1) % split cell and change phenotype
    case 'assymetric'
        split_cell(2)
    case 'dedifferentiate'
        lattice(position) = lattice(position) + stem_value; % only CC->CSC
        keep()
    case 'plasticity'
        state = rem(lattice(position),stem_value) > 0;
    otherwise
        disp(strcat('Action: ',action, ' not implemented yet'))
        state = 0;
end

    function move_cell()
        lattice(next_position) = lattice(position);
        lattice(position) = 0;
        next = next + 1;
        new_locations(next) = next_position;
    end

    function p = get_neighbour_location()
        
        p = 0;
        for neighbour = position + neighbourvec(randperm(8,8));
            if lattice(neighbour) == 0
                p = neighbour;
                
                break
            end
        end
    end

    function split_cell(change_phenotype)
        
        if change_phenotype == 1             % Differentiate CSC -> CC + CC
            new_value = lattice(position) - stem_value ;
            lattice(position) = new_value;    
        elseif change_phenotype == 2         % Assymetric  CSC -> CSC + CC
            new_value = lattice(position) - stem_value;
        elseif ~cell_action(position,'stem') % Symmetric, CC -> CC + CC
            % Decreasing rho
            new_value = lattice(position) - 1; 
            lattice(position) = new_value; 
        else                                 % Symmetric CSC -> CSC + CSC
            new_value = lattice(position);
        end

        keep()
        cell_action(next_position, 'add', new_value);
    end

    function keep()
        next = next + 1;
        new_locations(next) = position;
    end

end

