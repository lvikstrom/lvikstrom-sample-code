function cell_cycle(position)

% Performs a cell lifecycle
global  alpha p_p p_s p_d p_m p_dd next_position

if ~cell_action(position,'stem') && rand < alpha % spontaneous death
    % remove the cell from the grid
    %fprintf('remove \n')
    cell_action(position,'remove');
    return
end

next_position = cell_action(position,'get_neighbour');

if next_position % else we wont do anything
    
    if rand < p_p % proliferation attempt
        p = rand;
        if cell_action(position,'stem')                    % CSC currently
            % p_a = 1 - (p_s + p_d) % stem cell
            if  p < p_s
                % split into two stem cells (rho won't decrease)
                %fprintf('split CSC \n')
                cell_action(position,'split');
            elseif p < (p_s + p_d)
                % change self to CC and create additional CC
                %fprintf('differentiate CSC \n')
                cell_action(position,'differentiate');
            else
                % stay CSC & create additional CC
                %fprintf('assymetric CSC \n')
                cell_action(position,'assymetric');
            end
            
        else                                               % CC currently
            if ~ cell_action(position,'plasticity')
                % remove from grid
                %fprintf('remove CC \n')
                cell_action(position, 'remove');
            elseif p < (1 - p_dd)
                % create additional CC
                %fprintf('split CC \n')
                cell_action(position,'split');
            else
                % just change phenotype
                %fprintf('dedifferentiate CC \n')
                cell_action(position,'dedifferentiate');
            end
        end
        
    elseif rand < p_m % migrating
        %fprintf('moving \n')
        cell_action(position, 'move');
    else
        %fprintf('keeping \n')
        cell_action(position, 'keep');
    end
else
    cell_action(position, 'keep');
end

end