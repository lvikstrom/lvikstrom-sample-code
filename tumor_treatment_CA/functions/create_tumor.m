function [locations, lattice, neighbourvec] = create_tumor(m)

global stem_value rho
 
start_pos = floor(0.5*m + m^2/2); % in the middle..

locations(1) = start_pos;
lattice = zeros(m);
lattice(start_pos) = stem_value + rho;
neighbourvec = [-1 1 m-1 m m+1 1-m -m -m-1];

end