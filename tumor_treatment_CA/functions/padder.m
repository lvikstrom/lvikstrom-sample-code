function [lattice, locations, neighbourvec, did] = padder(lattice,locations)

% plot(1:24,(2/3*1/4).^(1:24))
did = 0;
padl = 10; padh = 3*padl;
pad = padh - padl;
m = size(lattice,1);
buffer = 3;

west = fix(min(locations)/m) - buffer;            % west
east = fix(max(locations)/m) + buffer;            % east
north = min(rem(locations,m)) - buffer;           % north
south = max(rem(locations,m)) + buffer;          % south

if any(locations) && (west < padl || west > padh || east < padl || ...
        east > padh || south < padl || south > padh || north < padl || north > padh)

    top_bottom = zeros(pad,2*pad + east - west + 1);
    sides = zeros(south-north+1,pad);
    lattice_cut = lattice(north:south,west:east);

    lattice = [top_bottom; [sides lattice_cut sides]; top_bottom];
    m = size(lattice,1);
    locations = find(lattice);
    did = 1;
end
neighbourvec = [-1 1 m-1 m m+1 1-m -m -m-1];
end