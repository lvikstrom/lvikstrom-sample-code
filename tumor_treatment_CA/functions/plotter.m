function h = plotter(lattice, varargin)
global stem_value rho

arg = varargin{1};
if strcmp(arg,'setup')
    cmap = [1 1 1; varargin{2}]; % color_map_choice
    n_colors = size(cmap,1);
    cmap = interp1(1:n_colors,cmap,linspace(1,n_colors,stem_value+rho));
    figure(1), clf, hold on, colormap(cmap), axis square tight
    h = pcolor(lattice);
    set(h, 'EdgeColor', 'none', 'CData',lattice);
    set(gca,'CLim',[0 stem_value+rho])
    hcb = colorbar;
    cc_ticks =  [0:5:rho];
    ticklabels = cellstr([
        num2str(cc_ticks', 'CC:  p=%-d');
        num2str(cc_ticks', 'CSC: p=%-d')]);
    set(hcb,'YTick',[cc_ticks cc_ticks+stem_value], ...
        'YTickLabel',ticklabels)

elseif strcmp(arg,'update_grid')
    h = pcolor(lattice);
    axis square tight 
    set(h, 'EdgeColor', 'none');
    drawnow;
else % update
    set(arg,'CData',lattice)
    drawnow;
end
end