function radio_therapy( quiscent_csc_SF, quiscent_cc_SF , csc_SF, cc_SF )

global locations

for position = locations
    r=rand;
    
    if cell_action(position,'get_neighbour')
        if cell_action(position,'stem')
            if r > csc_SF
                cell_action(position,'remove');
            end
        else
            if r > cc_SF
                cell_action(position,'remove');
            end
        end
    else
        if cell_action(position,'stem')
            if r > quiscent_csc_SF 
                cell_action(position,'remove');
            end
        else
            if r > quiscent_cc_SF
                cell_action(position,'remove');
            end
        end          
    end
end


end
