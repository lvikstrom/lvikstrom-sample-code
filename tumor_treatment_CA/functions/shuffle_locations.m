function rand_idx = shuffle_locations(n_active_locations) 
    rand_idx = randperm(n_active_locations, n_active_locations); 
end