clc, clearvars, clearvars -global 
global rho alpha p_s p_d p_dd p_p p_m xi lambda a b D ...
    lattice locations stem_value next new_locations neighbourvec
% ----------- Parameters -----------
% ----------------------------------
rho = 15;           % Nonstem (CC) proliferation capacity
alpha = 0.01;       % P( spontaneous death of CC )
p_s = 0.1;          % P( symmetriv division of CSC )
p_d = 0.01;         % P( differentiation )
p_dd = p_d;         % P( dedifferentiation )
p_p = 1/24;         % P( proliferation )
p_m = 15/24;        % P( migration | ~ proliferation )
xi = 0.5;           % Radioresistance | full neighbourhood
lambda = 0.1376;    % Radioresistance CSC
a = 0.3859;         % radiation response model (rrm) parameter (1/Gy)
b = 0.01148;        % rrm parameter (1/Gy^2)
%D = 2;              % radiation dose, Gy

% -------- Derived Parameters ------
% ----------------------------------
initial_csc = 1; old_toc = 0;
print_freq = 5;
simulation_time = 50 * 24; % n days * 24 hours
plot_freq = print_freq; 
cell_counts = zeros(3,simulation_time);
stem_value = 2*rho; next = 1;
[locations, lattice, neighbourvec] = create_tumor(20);

% -------- Radio Therapy -----------
% ----------------------------------
tumor_find_size = 1e3;            % we need to decide on this
total_therapy_times = 50;          % set
radiation_freq = 12;               % set
total_dose = 90;                   % set
current_therapy_times = 0;
D = total_dose / total_therapy_times;
[ quiscent_csc_SF, quiscent_cc_SF , csc_SF, cc_SF ] = SF( D ); %Radiation thresholds
tumor_found_time = simulation_time / 50;
% --------------- RNG --------------
% ----------------------------------
rng('shuffle')

% -------------- Plot --------------
% ----------------------------------
h = plotter(lattice, 'setup', parula);
set(gca,'nextplot','replacechildren'); 
% ----------- Simulation -----------
% ----------------------------------

tic
new_locations = zeros(1,round(1.1*tumor_find_size));
for t = 1 : simulation_time
    cell_counts(1,t) = next; % total amount of cells.
    order = randperm(next, next);
    next = 0;
    for idx = order
        cell_cycle(locations(idx))
    end
    locations = new_locations(1:next); 

    if  (next > tumor_find_size || current_therapy_times) && ...
            mod(t,radiation_freq) == 0 && ...
            current_therapy_times < total_therapy_times
        
        current_therapy_times = current_therapy_times + 1;

        radio_therapy( quiscent_csc_SF, quiscent_cc_SF , csc_SF, cc_SF );
        fprintf('Radiotherepy %.0f / %.0f applied \n', ...
            current_therapy_times,total_therapy_times)

    end
    
    if mod(t,radiation_freq) == 0
        [lattice, locations, neighbourvec,did] = padder(lattice, locations);
        next = length(locations);
        if did
            h=plotter(lattice,'update_grid');
        else
            fprintf('Skipped resizeing array \n');
        end
    elseif plot_freq && (mod(t, plot_freq) == 1 || plot_freq == 1)
        plotter(lattice,h);
    end
    
    if print_freq && mod(t, print_freq)==1;
        fprintf('Iteration %.0f / %.0f, %.0f cells, last %.0f iterations in %.3f s\n', ...
            t, simulation_time,next, print_freq, toc - old_toc)
        old_toc = toc;
        if next == 0
            fprintf('Tumor has vanished after %.0f iterations \n',t)
            break
        elseif current_therapy_times >= total_therapy_times && next > 10000
            fprintf('More than 10000 cancer cells after therapy \n')
            break
        end
    end
   
end
%%
figure(2),
semilogy(1:t,cell_counts(1,1:t),'Linewidth',3.0)
xlabel('Time (hours)')
ylabel('Cell count')
title('Cell count over a radio therapy simulation')
hold off